import Config

for config <- "/etc/asciinema/conf.d/*.exs" |> Path.wildcard() do
  import_config config
end
