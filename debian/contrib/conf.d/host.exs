import Config

# Used by the application to build urls. This must be the external access
# point.
config :asciinema, AsciinemaWeb.Endpoint,
  url: [
    scheme: "http",
    host: "localhost",
    path: "/asciinema",
    port: "80"
  ]
