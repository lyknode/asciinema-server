import Config

config :asciinema, Asciinema.FileStore.Local,
  path: "/var/lib/asciinema/uploads/"
